import React, { useState } from "react";

import { Wrapper, Container } from "./components";
import { Main, Info, Success } from "./parts";
import { orderApi } from "./api";

import { Bids, Info as InfoProps } from "./interfaces";

const App: React.FC = () => {
  const [currentStep, setCurrentStep] = useState<string>("main");

  const [bidsData, setBidsData] = useState<Bids>({});
  const [info, setInfo] = useState<InfoProps>({});

  const handleExchange = (burs: Bids, infoProps: InfoProps) => {
    setBidsData(burs);
    setCurrentStep("info");
    setInfo(infoProps);
  };

  const handleCancel = () => {
    setBidsData({});
    setCurrentStep("main");
  };

  const handleConfirm = () => {
    const { base, amount, invoicePayMethod, withdrawPayMethod } = bidsData;

    const data = {
      base,
      amount: Number(amount),
      invoicePayMethod: Number(invoicePayMethod),
      withdrawPayMethod: Number(withdrawPayMethod),
    };

    orderApi.send(data).then(({ message }) => {
      if (message === "Success") {
        setCurrentStep("success");
      }
    });
  };

  return (
    <Wrapper>
      <Container>
        {currentStep === "main" && <Main onExchange={handleExchange} />}
        {currentStep === "info" && (
          <Info info={info} onCancel={handleCancel} onConfirm={handleConfirm} />
        )}
        {currentStep === "success" && <Success onCancel={handleCancel} />}
      </Container>
    </Wrapper>
  );
};

export default App;
