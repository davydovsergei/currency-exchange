export type Type = "invoice" | "withdraw";

export interface Payment {
  id: number;
  name: string;
}

export interface PaymentsGroup {
  invoice: Payment[];
  withdraw: Payment[];
}

export interface QueryStringParams {
  base: Type;
  amount: number | string;
  invoicePayMethod: number | string;
  withdrawPayMethod: number | string;
}

export interface Inputs {
  payMethod: number | string;
  amount: string;
  autoChange: boolean;
  loading: boolean;
}

export interface Bids {
  base?: Type;
  amount?: number | string;
  invoicePayMethod?: number | string;
  withdrawPayMethod?: number | string;
}

export interface Info {
  invoice?: { amount: string; payMethod: string };
  withdraw?: { amount: string; payMethod: string };
}
