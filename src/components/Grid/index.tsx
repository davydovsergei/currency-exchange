import React from "react";

import styles from "./Grid.module.scss";

export const Row: React.FC = ({ children }) => (
  <div className={styles.Row}>{children}</div>
);

export const Col6: React.FC = ({ children }) => (
  <div className={styles.Col6}>{children}</div>
);
