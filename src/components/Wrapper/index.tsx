import React from "react";

import styles from "./Wrapper.module.scss";

export const Wrapper: React.FC = ({ children }) => (
  <div className={styles.Wrapper}>{children}</div>
);
