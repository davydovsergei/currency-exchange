import React from "react";

import { FormGroup, Select, InputText } from "../../ui";

import { Payment, Inputs } from "../../interfaces";

interface SellBuyProps {
  title: string;
  payments: Payment[];
  value: Inputs;
  onChange: (name: string) => (value: string) => void;
}

export const SellBuy: React.FC<SellBuyProps> = ({
  title,
  payments,
  value,
  onChange,
}) => {
  return (
    <>
      <h2>{title}</h2>
      <FormGroup>
        <Select
          value={value.payMethod}
          options={payments}
          onChange={(value) => onChange("payMethod")(value)}
        />
      </FormGroup>
      <FormGroup>
        <InputText
          value={value.amount}
          loading={value.loading}
          onChange={(value) => onChange("amount")(value)}
        />
      </FormGroup>
    </>
  );
};
