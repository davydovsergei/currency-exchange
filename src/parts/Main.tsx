import React, { useEffect, useState } from "react";

import { Row, Col6, SellBuy } from "../components";
import { ButtonCenter, Button } from "../ui";
import { payMethodsApi } from "../api";
import { calculateParamsConstructor } from "../utils";

import { PaymentsGroup, Bids, Inputs, Info, Payment } from "../interfaces";

interface MainProps {
  onExchange: (bids: Bids, info: Info) => void;
}

const initPayments = {
  invoice: [],
  withdraw: [],
};

const initInputs = {
  payMethod: "",
  amount: "1",
  autoChange: true,
  loading: false,
};

export const Main: React.FC<MainProps> = ({ onExchange }) => {
  const [payments, setPayments] = useState<PaymentsGroup>(initPayments);

  const [invoice, setInvoice] = useState<Inputs>(initInputs);
  const [withdraw, setWithdraw] = useState<Inputs>(initInputs);

  const [bidsData, setBidsData] = useState<Bids>({});

  useEffect(() => {
    payMethodsApi.getAll().then((res) => {
      setPayments(res);
    });
  }, []);

  useEffect(() => {
    if (payments.invoice.length !== 0 && payments.withdraw.length !== 0) {
      setInvoice((prev) => ({
        ...prev,
        payMethod: payments.invoice[0].id.toString(),
      }));

      setWithdraw((prev) => ({
        ...prev,
        payMethod: payments.withdraw[0].id.toString(),
      }));
    }
  }, [payments]);

  useEffect(() => {
    if (
      !invoice.autoChange &&
      invoice.amount !== "" &&
      invoice.payMethod !== ""
    ) {
      const params = calculateParamsConstructor(
        "invoice",
        invoice.amount,
        invoice.payMethod,
        withdraw.payMethod
      );

      setWithdraw((prev) => ({ ...prev, loading: true }));

      setBidsData(params);

      payMethodsApi.calculate(params).then(({ amount }) => {
        setWithdraw((prev) => ({
          ...prev,
          amount,
          autoChange: true,
          loading: false,
        }));
      });
    } // eslint-disable-next-line
  }, [invoice]);

  useEffect(() => {
    if (
      !withdraw.autoChange &&
      withdraw.amount !== "" &&
      withdraw.payMethod !== ""
    ) {
      const params = calculateParamsConstructor(
        "withdraw",
        withdraw.amount,
        withdraw.payMethod,
        invoice.payMethod
      );

      setInvoice((prev) => ({ ...prev, loading: true }));

      setBidsData(params);

      payMethodsApi.calculate(params).then(({ amount }) => {
        setInvoice((prev) => ({
          ...prev,
          amount,
          autoChange: true,
          loading: false,
        }));
      });
    } // eslint-disable-next-line
  }, [withdraw]);

  const handleChangeInvoice = (name: string) => (value: string) => {
    setInvoice((prev) => ({
      ...prev,
      autoChange: false,
      [name]: value,
    }));

    setWithdraw((prev) => ({
      ...prev,
      autoChange: true,
    }));
  };

  const handleChangeWithdraw = (name: string) => (value: string) => {
    setWithdraw((prev) => ({
      ...prev,
      autoChange: false,
      [name]: value,
    }));

    setInvoice((prev) => ({
      ...prev,
      autoChange: true,
    }));
  };

  const handleExchange = () => {
    if (Object.keys(bidsData).length !== 0) {
      const findPayments = (
        payMethods: Payment[],
        payMethod: string | number
      ) => payMethods.find(({ id }) => id.toString() === payMethod)?.name || "";

      const info = {
        invoice: {
          amount: invoice.amount,
          payMethod: findPayments(payments.invoice, invoice.payMethod),
        },
        withdraw: {
          amount: withdraw.amount,
          payMethod: findPayments(payments.withdraw, withdraw.payMethod),
        },
      };

      onExchange(bidsData, info);
    } else {
      alert("Please fill in all required fields");
    }
  };

  return (
    <>
      <Row>
        <Col6>
          <SellBuy
            title="Sell"
            payments={payments.invoice}
            value={invoice}
            onChange={handleChangeInvoice}
          />
        </Col6>
        <Col6>
          <SellBuy
            title="Buy"
            payments={payments.withdraw}
            value={withdraw}
            onChange={handleChangeWithdraw}
          />
        </Col6>
      </Row>
      <ButtonCenter>
        <Button value="Exchange" onClick={handleExchange} />
      </ButtonCenter>
    </>
  );
};
