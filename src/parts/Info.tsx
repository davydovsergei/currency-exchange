import React, { useState } from "react";

import { Table, ButtonCenter, Button, Button2 } from "../ui";

import { Info as InfoData } from "../interfaces";

interface InfoProps {
  info: InfoData;
  onCancel: () => void;
  onConfirm: () => void;
}

export const Info: React.FC<InfoProps> = ({ info, onCancel, onConfirm }) => {
  const [loadingNextStep, setLoadingNextStep] = useState(false);

  const row = [
    {
      name: "Sell",
      value: `${info.invoice?.amount} ${info.invoice?.payMethod}`,
    },
    {
      name: "Buy",
      value: `${info.withdraw?.amount} ${info.withdraw?.payMethod}`,
    },
  ];

  const handleConfirm = () => {
    setLoadingNextStep(true);
    onConfirm();
  };

  return (
    <>
      <h2>Details</h2>
      <Table row={row} />
      <ButtonCenter>
        <Button2 value="Cancel" onClick={onCancel} />
        <Button
          value="Confirm"
          loading={loadingNextStep}
          onClick={handleConfirm}
        />
      </ButtonCenter>
    </>
  );
};
