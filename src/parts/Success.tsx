import React from "react";

import { ButtonCenter, Button } from "../ui";
import { Success as SuccessIco } from "../assets/svg";

interface SuccessProps {
  onCancel: () => void;
}

export const Success: React.FC<SuccessProps> = ({ onCancel }) => (
  <>
    <div className="center ico-block">
      <SuccessIco />
    </div>
    <h3 className="center">Success!</h3>
    <p className="center">
      Your exchange order has been placed <br />
      successfully and will be processed soon.
    </p>
    <ButtonCenter>
      <Button value="Home" onClick={onCancel} />
    </ButtonCenter>
  </>
);
