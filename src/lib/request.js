import axios from "axios";

const BASE_URL = process.env.REACT_APP_API_BASE_URL;
const isDev = process.env.NODE_ENV !== "production";

const contentTypeFromOptions = (options) => {
  const contentType = options?.headers && options.headers["Content-Type"];
  if (contentType) return contentType;

  return typeof options.body === "object"
    ? "application/json"
    : contentType || "";
};

const withContentType = (options) => {
  const header = contentTypeFromOptions(options);

  return header ? { "Content-Type": header } : {};
};

const successHandler = (response) => {
  const contentType = response.headers["content-type"];
  if (response.status === 401) return response;
  if (contentType.includes("json")) {
    return response.data;
  }
  return response;
};

const errorHandler = (error) => {
  console.log(error);
};

export const request = (method, routePath, requestOptions = {}) => {
  const headers = {
    ...withContentType(requestOptions),
    ...requestOptions.headers,
  };

  const { body, baseUrl = BASE_URL, ...restOptions } = requestOptions;
  const fullRoute = `/api/${routePath}`;

  const config = {
    ...restOptions,
    url: fullRoute,
    baseURL: baseUrl,
    method,
    headers,
    data: body,
  };

  if (isDev) {
    console.groupCollapsed(`API -> ${config.method} ${config.url}`);
    console.log("request:", config);
    console.groupEnd();
  }

  return axios(config)
    .then((response) => successHandler(response))
    .catch((e) => {
      if (e?.response?.status === 400) {
        return { errors: e?.response?.data };
      }

      return errorHandler(e);
    });
};
