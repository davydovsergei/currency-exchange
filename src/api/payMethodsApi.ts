import { request } from "../lib/request";

import { QueryStringParams } from "../interfaces";

const getAll = () =>
  request("GET", "payMethods")
    .then((res) => res)
    .catch((err) => err);

const calculate = (params: QueryStringParams) =>
  request("GET", "payMethods/calculate", { params })
    .then((res) => res)
    .catch((err) => err);

export const payMethodsApi = { getAll, calculate };
