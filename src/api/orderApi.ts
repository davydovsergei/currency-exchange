import { request } from "../lib/request";

import { QueryStringParams } from "../interfaces";

const send = (params: {} | QueryStringParams) =>
  request("POST", "bids", { body: params })
    .then((res) => res)
    .catch((err) => err);

export const orderApi = { send };
