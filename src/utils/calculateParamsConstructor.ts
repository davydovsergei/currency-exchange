export const calculateParamsConstructor = (
  base: "invoice" | "withdraw",
  amount: number | string,
  invoicePayMethod: number | string,
  withdrawPayMethod: number | string
) => ({
  base,
  amount,
  invoicePayMethod,
  withdrawPayMethod,
});
