import React from "react";

import styles from "./Table.module.scss";

interface Row {
  name: string;
  value: string;
}

interface TableProps {
  row: Row[];
}

export const Table: React.FC<TableProps> = ({ row = [] }) => (
  <table className={styles.Table}>
    <tbody>
      {row.map(({ name, value }, index) => (
        <tr key={index}>
          <td>{name}</td>
          <td>{value}</td>
        </tr>
      ))}
    </tbody>
  </table>
);
