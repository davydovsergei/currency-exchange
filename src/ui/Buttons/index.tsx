import React from "react";

import { Loader } from "../Loader";

import styles from "./Buttons.module.scss";

interface ButtonProps {
  value: string;
  loading?: boolean;
  onClick: () => void;
}

export const Button: React.FC<ButtonProps> = ({
  value,
  loading = false,
  onClick,
}) => (
  <button className={styles.Button} type="button" onClick={onClick}>
    {loading ? <Loader /> : value}
  </button>
);

export const Button2: React.FC<ButtonProps> = ({
  value,
  loading = false,
  onClick,
}) => (
  <button className={styles.Button2} type="button" onClick={onClick}>
    {loading ? <Loader /> : value}
  </button>
);
