import React from "react";

import styles from "./ButtonCenter.module.scss";

export const ButtonCenter: React.FC = ({ children }) => (
  <div className={styles.ButtonCenter}>{children}</div>
);
