import React from "react";

import { Loader } from "../Loader";

import styles from "./InputText.module.scss";

interface InputTextProps {
  type?: string;
  value: string;
  loading?: boolean;
  onChange: (value: string) => void;
}

export const InputText: React.FC<InputTextProps> = ({
  type = "text",
  value = "",
  loading = false,
  onChange,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const isValid = /^[0-9]\d*\.?\d*$/.test(event.target.value);

    if (event.target.value === "" || isValid) {
      onChange(event.target.value);
    }
  };

  return (
    <div className={styles.Container}>
      <input
        className={styles.InputText}
        type={type}
        value={value}
        onChange={handleChange}
      />
      {loading && (
        <div className={styles.LoaderContainer}>
          <Loader />
        </div>
      )}
    </div>
  );
};
