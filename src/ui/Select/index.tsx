import React from "react";

import styles from "./Select.module.scss";

interface OptionProps {
  id: number | string;
  name: string;
}

interface SelectProps {
  options: OptionProps[];
  value: number | string;
  onChange: (value: string) => void;
}

export const Select: React.FC<SelectProps> = ({ options, value, onChange }) => {
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(event.target.value);
  };

  return (
    <select value={value} className={styles.Select} onChange={handleChange}>
      {options.map(({ id, name }) => (
        <option key={id} value={id}>
          {name}
        </option>
      ))}
    </select>
  );
};
